
/**
 * A simple program that demos Pattern Matcher
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */
import java.util.regex.*;

public class RegexChecker {
    public static void main(String[] args) {
        String longStr = " Daniel Cisneros 555-1234 CO Blood-Donor myemail@gmail.com libra";
        String strangeStr = "";
        String romanStr = "IX";

        // Solve a roman numeral for using IVX
        /**
         * I, II, III, IV, V, VI, VII, VIII, IX
         */
        regexCheck("^(IX|IV|V?I{0,3})$", romanStr);

    }

    public static void regexCheck(String theRegex, String str2Check) {
        Pattern checkRegex = Pattern.compile(theRegex);
        Matcher regexMatcher = checkRegex.matcher(str2Check);

        while (regexMatcher.find()) {
            if (regexMatcher.group().length() != 0) {
                System.out.println(regexMatcher.group().trim());
            }
            System.out.println("Start index: " + regexMatcher.start());
            System.out.println("End index: " + regexMatcher.end());
        }
        if (regexMatcher.matches() == false) {
            System.out.println("No Matches Found!");
        }
    }
}